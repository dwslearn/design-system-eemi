/* 
Capter la balise body dans une constante
*/
    const bodyTag = document.querySelector('body');
//

/* 
Fonction pour capter la largeur de la page
*/
    const resizeClass = () => {
        if( window.innerWidth < 375 ){
            // Supprimer toutes les classe
            bodyTag.classList.remove('small-theme', 'medium-theme', 'large-theme', 'x-large-theme')

            // Ajouter la boone classe
            bodyTag.classList.add('small-theme')
        }
        else if( 
            window.innerWidth > 375 &&
            window.innerWidth < 750
        ){
            // Supprimer toutes les classe
            bodyTag.classList.remove('small-theme', 'medium-theme', 'large-theme', 'x-large-theme')

            // Ajouter la boone classe
            bodyTag.classList.add('medium-theme')
        }
        else if(
            window.innerWidth > 950
        ){
            // Supprimer toutes les classe
            bodyTag.classList.remove('small-theme', 'medium-theme', 'large-theme', 'x-large-theme')

            // Ajouter la boone classe
            bodyTag.classList.add('large-theme')
        }
    }
//

/* 
Capter un événement
*/
    window.addEventListener( 'resize', (event) => {
        resizeClass();
    })
/**/

resizeClass()